﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseInitApp.Interfaces
{
    interface IWarehouseInitializer
    {
        int addItem(string itemString);
        string getWarehouseContentString();
    }
}
