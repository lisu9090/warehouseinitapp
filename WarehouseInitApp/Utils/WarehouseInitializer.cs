﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseInitApp.Interfaces;
using WarehouseInitApp.Models;

namespace WarehouseInitApp.Utils
{
    public class WarehouseInitializer : IWarehouseInitializer
    {
        private List<WarehouseAbstract> warehouses = new List<WarehouseAbstract>();
        public int addItem(string itemString)
        {
            if (!itemString.Trim().StartsWith("#"))
            {
                var data = itemString.Split(";");
                ItemAbstract item = new ItemModel(data[1], data[0]);

                var itemDistribution = getItemDistribution(data[2]);

                return createAndUpdateWarehouses(item, itemDistribution);
            }

            return 0;
        }

        public string getWarehouseContentString()
        {
            StringBuilder builder = new StringBuilder();

            warehouses.Sort((x, y) => {
                int tmp = y.getItemsTotalQuantity() - x.getItemsTotalQuantity();
                return tmp == 0 ? y.name.CompareTo(x.name) : tmp;
            });

            foreach(var warehouse in warehouses)
            {
                builder.Append(parseWarehouseDataToString(warehouse));
            }

            return builder.ToString();
        }

        private IEnumerable<Tuple<string, int>> getItemDistribution(string itemQuantity)
        {
            string[] quantities = itemQuantity.Split("|");
            string[] tmp;
            var toReturn = new List<Tuple<string, int>>();

            foreach(var quant in quantities)
            {
                tmp = quant.Split(",");
                toReturn.Add(new Tuple<string, int>(tmp[0], int.Parse(tmp[1]))); //Using Parese instead of TryPare because of assumptions 
            }

            return toReturn;
        }

        private int createAndUpdateWarehouses(ItemAbstract item, IEnumerable<Tuple<string, int>> itemDistribution)
        {
            int itemCount = 0; //this counter contains total quatity of inserted item (in all warehouses) 

            WarehouseAbstract warehouse;

            foreach (var distr in itemDistribution)
            {
                warehouse = warehouses.Find(x => x.name.Equals(distr.Item1));

                if (warehouse == null)
                {
                    warehouse = new WarehouseModel(distr.Item1);
                    warehouses.Add(warehouse);
                }

                itemCount += distr.Item2;
                item.quantity = distr.Item2;
                warehouse.addWarehouseItem((ItemAbstract)item.Clone());
            }

            return itemCount;
        }

        private string parseWarehouseDataToString(WarehouseAbstract warehouse)
        {
            StringBuilder builder = new StringBuilder(); //boost operations on string

            builder.Append(warehouse.name + " (total " + warehouse.getItemsTotalQuantity() + ")\n");

            ((List<ItemAbstract>)warehouse.getWarehouseItems()).Sort((x, y) => {
                return x.id.CompareTo(y.id);
            });

            foreach(var item in warehouse.getWarehouseItems())
            {
                builder.Append(item.id + ": " + item.quantity + "\n");
            }

            builder.Append("\n");
            return builder.ToString();
        }
    }
}
