﻿using System;
using WarehouseInitApp.Interfaces;
using WarehouseInitApp.Utils;

namespace WarehouseInitApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Warehouse initialization has been started! Type 'print' to print wharehouses content or 'exit' to termiante this program. \n\n");

            IWarehouseInitializer initializer = new WarehouseInitializer();

            try
            {
                string userInput;
                while (true)
                {
                    userInput = Console.ReadLine();

                    switch (userInput.Trim().ToLower())
                    {
                        case "exit":
                            return;
                        case "print":
                            Console.Write(initializer.getWarehouseContentString());
                            break;
                        case "":
                            Console.WriteLine();
                            break;
                        default:
                            Console.WriteLine(initializer.addItem(userInput) + " item(s) added.");
                            break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
