﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseInitApp.Models
{
    class ItemModel: ItemAbstract
    {
        public ItemModel(string id, string name) : base(id, name) { }

        public override object Clone()
        {
            var tmp = new ItemModel(id, name);
            tmp.quantity = quantity;

            return tmp;
        }
    }
}
