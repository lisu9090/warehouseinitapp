﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseInitApp.Models
{
    public class WarehouseModel : WarehouseAbstract
    {
        public WarehouseModel(string name) : base(name) { }
        public override void addWarehouseItem(ItemAbstract item)
        {
            var tmp = items.Find(x => x.id.Equals(item.id));

            if(tmp != null)
                tmp.quantity += item.quantity;
            else
                items.Add(item);
        }

        public override IEnumerable<ItemAbstract> getWarehouseItems()
        {
            return items;
        }

        public override int getItemsTotalQuantity()
        {
            int toReturn = 0;

            foreach(var item in items)
            {
                toReturn += item.quantity;
            }

            return toReturn;
        }
    }
}
