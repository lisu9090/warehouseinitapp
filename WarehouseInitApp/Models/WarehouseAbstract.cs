﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseInitApp.Models
{
    public abstract class WarehouseAbstract
    {
        public readonly string name;
        protected List<ItemAbstract> items = new List<ItemAbstract>();

        protected WarehouseAbstract(string name)
        {
            this.name = name;
        }

        public abstract void addWarehouseItem(ItemAbstract item);

        public abstract IEnumerable<ItemAbstract> getWarehouseItems();

        public abstract int getItemsTotalQuantity();
    }
}
