﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseInitApp.Models
{
    public abstract class ItemAbstract : ICloneable
    {
        public readonly string id;
        public readonly string name;
        public int quantity;

        protected ItemAbstract(string id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public abstract object Clone();
    }
}
